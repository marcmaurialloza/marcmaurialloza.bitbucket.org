$(document).ready(function() {
  
  // Start from scratch on page load
  if(window.location.hash) {
    window.location = "index.html";
  }
  
  // Feature tests and settings
  var hasLocalStorage = "localStorage" in window,
    maxHistory = 10;
  
  // List of elements we'll use
  var $taskForm = $("#taskForm"),
    $taskNameInput = $("#taskNameInput"),
     
    $tasksContainer = $("#tasksContainer");
  
  // Create an application object
  app = {
     
    // App initialization
    init: function() {
      var self = this;
       
      // Focus on the create task box
      focusOnTaskBox();
       
      // Add the form submission event
      $taskForm.on("submit", onFormSubmit);
       
      // Show tasks if there are items there
      this.tasks.init();
       
      // Clear tasks when button clicked
      $("#deleteAllTasksButton").on("click", function(e) {
        e.preventDefault();
        localStorage.removeItem("tasks");
        self.tasks.hideList();
      })
    },
     
    // Tasks modules
    tasks: {
      $listNode: $("#taskList"),
      $blockNode: $("#homePrev"),
      init: function() {
        var tasks = this.getItemsFromTasks(),
          self = this;
         
        // Add items to the list
        if(tasks.length) {
          tasks.forEach(function(item) {
            self.addItemToList(item);
          });
          self.showList();
        }
         
        // Use event delegation to look for list items clicked
        this.$listNode.delegate("a", "click", function(e) {
          $taskNameInput.val(e.target.textContent);
          onFormSubmit();
        });
      },
      getItemsFromTasks: function() {
        var tasks = "";
         
        if(hasLocalStorage) {
          tasks = localStorage.getItem("tasks");
        } else {
			alert("no localstorage");
		}
         
        return tasks ? JSON.parse(tasks) : [];
      },
      addItemToList: function(text, addToTop) {
        var $li = $("<li><a href='#'>" + text + "</a></li>"),
          listNode = this.$listNode[0];
           
        if(addToTop && listNode.childNodes.length) {
          $li.insertBefore(listNode.childNodes[0]);
        }
        else {
          $li.appendTo(this.$listNode);
        }
         
        this.$listNode.listview("refresh");
      },
      addItemToTasks: function(text, addListItem) {
        var currentItems = this.getItemsFromTasks(),
          newTasks = [text],
          self = this,
          found = false;
         
        // Cycle through the tasks, see if this is there
        $.each(currentItems, function(index, item) {
          if(item.toLowerCase() != text.toLowerCase()) {
            newTasks.push(item);
          }
          else {
            // We've hit a "repeater": signal to remove from list
            found = true;
            self.moveItemToTop(text);
          }
        });
         
        // Add a new item to the top of the list
        if(!found && addListItem) {
          this.addItemToList(text, true);
        }
         
        // Limit tasks to 10 items
        if(newTasks.length > maxHistory) {
          newTasks.length = maxHistory;
        }
         
        // Set new history
        if(hasLocalStorage) {
          // Wrap in try/catch block to prevent mobile safari issues with private browsing
          // http://frederictorres.blogspot.com/2011/11/quotaexceedederr-with-safari-mobile.html
          try {
            localStorage.setItem("tasks", JSON.stringify(newTasks));
          }
          catch(e){}
        }
         
        // Show the list
        this.showList();
      },
      showList: function() {
        $tasksContainer.addClass("fadeIn");
        this.$listNode.listview("refresh");
      },
      hideList: function() {
        $tasksContainer.removeClass("fadeIn");
		this.$listNode.hide();
      },
      moveItemToTop: function(text) {
        var self = this,
          $listNode = this.$listNode;
         
        $listNode.children().each(function() {
          if($.trim(this.textContent.toLowerCase()) == text.toLowerCase()) {
            $listNode[0].removeChild(this);
            self.addItemToList(text, true);
          }
        });
         
        $listNode.listview("refresh");
      }
    }
     
  };
  
  // New task submission
  function onFormSubmit(e) {
    if(e) e.preventDefault();
     
    // Trim the value
    var value = $.trim($taskNameInput.val());

    if(value) {
       
      // Add the search to tasks
      app.tasks.addItemToTasks(value, true);
      
    }
    else {
      // Focus on the search box
      focusOnTaskBox();
    }
     
    return false;
  }
  
  // Template substitution
  function substitute(str, obj) {
    return str.replace((/\\?{([^{}]+)}/g), function(match, name){
      if (match.charAt(0) == '\\') return match.slice(1);
      return (obj[name] != null) ? obj[name] : "";
    });
  }
  
  // Focuses on the input box
  function focusOnTaskBox() {
    $taskNameInput[0].focus();
  }
  
  // Modal function
  function showDialog(title, message) {
    $("#errorDialog h2.error-title").html(title);
    $("#errorDialog p.error-message").html(message);
    $.mobile.changePage("#errorDialog");
  }
  
  // Initialize the app
  app.init();
  
});
