
function error(name) {
  alert("Error:" + name);
}

if (window.navigator.mozApps) {
  var selfRequest = window.navigator.mozApps.getSelf();
  selfRequest.onsuccess = function () {
    if (window.navigator.onLine && !selfRequest.result) {
      var installRequest = window.navigator.mozApps.install("http://marcmaurialloza.bitbucket.org/manifest.webapp");
      installRequest.onsuccess = function () {
        alert("Installation suceed");
      };
      installRequest.onerror = function () {
        error(installRequest.error.name);
      };
    }
  };
  selfRequest.onerror = function () {
    error(selfRequest.error.name);
  };
}
